import json
import _pickle as pickle
from database import *


def get_boss_crystal_stats():
    # Get Weekly
    data = run_boss_crystal()
    weekly_meso = data[0]
    crystals = data[1]

    # Fill with daily
    data = run_boss_crystal(False, crystals, weekly_meso)
    mesos = data[0]

    return [crystals, (180 - crystals), "{:,}".format(mesos), "{:,}".format(weekly_meso)]


def run_boss_crystal(weekly=True, crystals=0, mesos=0):
    data = json.load(open('resources/tasks.json'))
    lists = Lists.select()

    if not weekly:
        crystals = 180 - crystals

    # Loop through lists
    for li in lists:
        if li.settings is None:
            continue

        settings = pickle.loads(li.settings)
        for task_list, values in settings.items():
            if task_list == 'Custom Tasks':
                continue
            day = values.get('day')

            tasks = values.get('tasks')
            for task, difficulty in tasks.items():
                meso = data.get(task_list).get('tasks').get(task)
                if meso is None:
                    continue
                if isinstance(meso, dict):
                    meso = meso.get(difficulty)
                if day == -1 and not weekly:
                    if crystals < 7 and crystals != 0:
                        mesos += (meso * crystals)
                        crystals -= crystals
                    else:
                        mesos += (meso * 7)
                        crystals -= 7
                elif weekly:
                    mesos += meso
                    crystals += 1

    return [mesos, crystals]


def get_task_amounts():
    lists = Lists.select()
    daily = 0
    monday = 0
    thursday = 0

    # Loop through lists
    for li in lists:
        if li.settings is None:
            continue

        settings = pickle.loads(li.settings)
        for task_list, values in settings.items():
            day = values.get('day')
            tasks = values.get('tasks')

            # Check if task is a checklist
            if values.get('checklist'):
                match day:
                    case -1:
                        daily += 1
                    case 0:
                        monday += 1
                    case 3:
                        thursday += 1
                continue

            # Handle custom tasks
            if task_list == 'Custom Tasks':
                for task_name, e in tasks.items():
                    custom_task = CustomTasks.select().where(CustomTasks.name == task_name).get()
                    match custom_task.day:
                        case 'Daily':
                            daily += 1
                        case 'Monday':
                            monday += 1
                        case 'Thursday':
                            thursday += 1
                continue

            # Handle rest of tasks
            match day:
                case -1:
                    daily += len(tasks)
                case 0:
                    monday += len(tasks)
                case 3:
                    thursday += len(tasks)

    return [daily, monday, thursday]
