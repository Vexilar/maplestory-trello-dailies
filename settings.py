import PySimpleGUI as sg
import trello_api
from database import *


def make_window():
    settings = [
        [sg.Text('Trello Settings', font=('Helvetica', 20), pad=(130, 1))],
        [sg.Text('Trello API Key', size=(14, 1)), sg.InputText(key='-TRELLO API-')],
        [sg.Text('Trello Token', size=(14, 1)), sg.InputText(key='-TRELLO TOKEN-')],
        [sg.Text('Trello Board ID', size=(14, 1)), sg.InputText(key='-TRELLO BOARD ID-')],
        [sg.Text('Stopwatch 1 Name', size=(14, 1)), sg.InputText(key='-STOPWATCH 1-')],
        [sg.Text('Stopwatch 2 Name', size=(14, 1)), sg.InputText(key='-STOPWATCH 2-')],
        [sg.Button("Save", key='-SAVE-'), sg.Button('Refresh Trello', key='-TRELLO JSON-',
                                                    tooltip='Press this button to load your board\'s json file so the program can automatically load all info.')]
    ]
    layout = [[sg.Column(settings)]]

    return sg.Window('Settings', layout)


def open_settings():
    window = make_window()

    try:
        settings = GeneralSettings.select().get()
    except:
        settings = GeneralSettings.create()

    # Update settings window
    window.read(timeout=1)
    window['-TRELLO API-'].update(settings.trello_api)
    window['-TRELLO TOKEN-'].update(settings.trello_token)
    window['-TRELLO BOARD ID-'].update(settings.board_id)
    window['-STOPWATCH 1-'].update(settings.stopwatch_one_name)
    window['-STOPWATCH 2-'].update(settings.stopwatch_two_name)

    while True:
        event, values = window.read()

        if event == '-SAVE-':
            result = save_settings(settings, values)
            if result:
                window.close()
                break
            elif result is None:
                sg.popup('Saved!')
        elif event == '-TRELLO JSON-':
            # Todo: Add check to make sure all req fields are enabled.
            if update_app(settings):
                settings = GeneralSettings.select().get()
                window['-TRELLO API-'].update(settings.trello_api)
                window['-TRELLO TOKEN-'].update(settings.trello_token)
                window['-TRELLO BOARD ID-'].update(settings.board_id)

        if event == 'OK' or event == sg.WIN_CLOSED:
            break

    window.close()
    return True


def save_settings(settings, values):
    # Check if it was the first run
    if settings.trello_api is None and settings.trello_token is None and settings.board_id is None:
        if values['-TRELLO API-'] == '' or values['-TRELLO TOKEN-'] == '' or values['-TRELLO BOARD ID-'] == '':
            sg.popup_error('Please fill in all settings.')
            return False
        else:
            settings = save(settings, values)
            update_app(settings)
            return True
    if values['-TRELLO API-'] == '' or values['-TRELLO TOKEN-'] == '' or values['-TRELLO BOARD ID-'] == '':
        sg.popup_error('Please fill in all settings.')
        return False
    else:
        save(settings, values)
    return None


def save(settings, values):
    settings.trello_api = values['-TRELLO API-']
    settings.trello_token = values['-TRELLO TOKEN-']
    settings.board_id = values['-TRELLO BOARD ID-']
    settings.stopwatch_one_name = values['-STOPWATCH 1-']
    settings.stopwatch_two_name = values['-STOPWATCH 2-']
    settings.save()
    return settings


def update_app(settings):
    board = trello_api.get_board()
    lists = trello_api.get_lists()
    labels = trello_api.get_labels()

    settings.board_url = board.get('url')
    settings.save()

    for li in lists:
        list_id = li.get('id')
        list_name = li.get('name')
        try:
            trello_list = Lists.select().where(Lists.list_id == list_id).get()
            trello_list.name = list_name
            trello_list.save()
        except:
            Lists.create(list_id=list_id, name=list_name)

    # Remove all labels
    Labels.delete().where(Labels.label_id != '').execute()

    # Grab labels from json then insert labels into database
    for label in labels:
        if label.get('name') != '':
            Labels.replace(label_id=label.get('id'), name=label.get('name')).execute()

    sg.popup('Done!')
    return True
