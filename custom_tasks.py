import PySimpleGUI as sg
import database
from database import *

selected_task = None


def make_window():
    tasks = [
        [sg.Text('Custom Tasks')],
        [sg.Listbox(values=database.get_custom_tasks(), enable_events=True, size=(40, 20), key="-TASKS-")],
        [sg.Button('Create Task', key='-CREATE TASK-'),
         sg.Button('Delete Task', key='-DELETE TASK-', button_color='white on red')]
    ]

    edit_task = [
        [sg.Text('Select Task', font=('Helvetica', 20), key='-TASK-')],
        [sg.Text('Run Day', size=(10, 1)), sg.Combo(['Daily', 'Monday', 'Thursday'], size=(15, 1), key='-DAY-')],
        [sg.Text('Trello Label', size=(10, 1)),
         sg.Combo(['None'] + database.get_labels(), size=(15, 1), key='-LABEL-')],
        [sg.Text('Card Position', size=(10, 1), tooltip='The position of the card when created on the list.'),
         sg.Combo(['None', 'Top', 'Bottom'], size=(15, 1), key='-POS-')],
        [sg.Button('Save Task', key='-SAVE-', button_color='white on green')]
    ]

    layout = [
        [
            sg.Column(tasks, vertical_alignment='top'),
            sg.VSeparator(),
            sg.Column(edit_task, vertical_alignment='top')
        ]
    ]

    return sg.Window('Custom Tasks', layout)


def open_tasks():
    global selected_task
    window = make_window()

    while True:
        event, values = window.read()

        if event == '-CREATE TASK-':
            if create_task():
                window['-TASKS-'].update(values=database.get_custom_tasks())
        elif event == '-DELETE TASK-':
            if selected_task is None:
                sg.popup_error('You must select a task first')
                continue
            response = sg.popup_ok_cancel('You are about to delete the list "%s"' % selected_task.name)
            if response == 'OK':
                CustomTasks.delete().where(CustomTasks.name == selected_task.name).execute()
                window['-TASKS-'].update(values=database.get_custom_tasks())
                selected_task = None
        elif event == '-TASKS-':
            try:
                selected_task = CustomTasks.select().where(CustomTasks.name == values['-TASKS-'][0]).get()

                label = ''
                pos = ''
                if selected_task.label_id is not None:
                    label = Labels.select().where(Labels.label_id == selected_task.label_id).get().name
                if selected_task.pos is not None:
                    pos = selected_task.pos

                window['-TASK-'].update(selected_task.name)
                window['-DAY-'].update(selected_task.day)
                window['-POS-'].update(pos.capitalize())
                window['-LABEL-'].update(label)
            except:
                selected_task = None
        elif event == '-SAVE-':
            if selected_task is None:
                sg.popup_error('You must select a task first')
                continue

            selected_task.day = values['-DAY-']

            if values['-LABEL-'] != '' and values['-LABEL-'] != 'None':
                selected_task.label_id = Labels.select().where(Labels.name == values['-LABEL-']).get().label_id

            if values['-POS-'] != '' and values['-POS-'] != 'None':
                selected_task.pos = values['-POS-'].lower()
            selected_task.save()
            sg.popup('Saved')
        if event == 'OK' or event == sg.WIN_CLOSED:
            break

    window.close()


def create_task():
    create_task_layout = [
        [sg.Text("Task Name:"), sg.InputText()],
        [sg.Text("Run Day:"), sg.Combo(['Daily', 'Monday', 'Thursday'])],
        [sg.Button('Create'), sg.Button('Cancel')]
    ]

    window = sg.Window('Create Task', create_task_layout)

    while True:
        event, values = window.read()

        if event == sg.WIN_CLOSED or event == 'Cancel':  # if user closes window or clicks cancel
            break

        if values[1] == '':
            sg.popup_error('You must select a day that the task will run')
        else:
            window.close()
            CustomTasks.create(name=values[0], day=values[1])
            return True

    window.close()
    return False

