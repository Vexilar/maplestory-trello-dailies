import PySimpleGUI as sg
import settings
import list_settings
import custom_tasks
import scripts
import webbrowser
import statistics
from datetime import datetime, timedelta

import utli
from database import *
import database

selected_list = None
sett = None


def make_window():
    top_nav_bar_left = [
        [sg.Text("Server Time"), sg.Text('', key='-SERVER TIME-')],
        [sg.Text("Ursus in 0:00:00", key='-URSUS-')],
    ]

    top_nav_bar_middle = [
        [sg.Text("Daily Reset", size=(11, 1)), sg.Text('', key='-DAILY RESET-')],
        [sg.Text("Monday Reset", size=(11, 1)), sg.Text('', key='-MONDAY RESET-')],
        [sg.Text("Thursday Reset", size=(11, 1)), sg.Text('', key='-THURSDAY RESET-')]
    ]

    top_nav_bar_right = [
        [sg.Button('Custom Tasks', key='-CUSTOM TASKS-'), sg.Button('Settings', key='-SETTINGS-'),
         sg.Button('Open Trello', key='-OPEN TRELLO-')]
    ]

    lists = [
        [sg.Text("Lists")],
        [sg.Listbox(values=[], enable_events=True, size=(40, 20), key="-LISTS-")],
        [sg.Button('Delete List', key='-DELETE LIST-', button_color='white on red'),
         sg.Button("Settings", key='-LIST SETTINGS-')]
    ]

    stats = [
        sg.Column(
            [
                [sg.Text('Boss Crystals Used', size=(18, 1)), sg.Text('0', key='-BOSS CRYSTAL USED-')],
                [sg.Text('Boss Crystals Remaining', size=(18, 1)), sg.Text('180', key='-BOSS CRYSTAL REMAINING-')],
                [sg.Text('Weekly Boss Mesos', size=(18, 1)), sg.Text('0', key='-BOSS MESOS-')],
                [sg.Text('Weekly Mesos', size=(18, 1)), sg.Text('0', key='-MESOS-')]
            ]
        ),
        sg.VSeparator(),
        sg.Column(
            [
                [sg.Text('Daily Tasks', size=(11, 1)), sg.Text('0', key='-DAILY TASKS-')],
                [sg.Text('Monday Tasks', size=(11, 1)), sg.Text('0', key='-MONDAY TASKS-')],
                [sg.Text('Thursday Tasks', size=(11, 1)), sg.Text('0', key='-THURSDAY TASKS-')]
            ]
        )
    ]

    stopwatch = [
        sg.Column(
            [
                [sg.Text('Timer 1', key='-STOPWATCH 1 NAME-'), sg.Text('0:00:00', key='-STOPWATCH 1-'),
                 sg.Button('Start', key='-STOPWATCH 1 START-'), sg.Button('Stop', key='-STOPWATCH 1 STOP-'),
                 sg.Button('Restart', key='-STOPWATCH 1 RESTART-')],
                [sg.Text('Timer 1', key='-STOPWATCH 2 NAME-'), sg.Text('0:00:00', key='-STOPWATCH 2-'),
                 sg.Button('Start', key='-STOPWATCH 2 START-'), sg.Button('Stop', key='-STOPWATCH 2 STOP-'),
                 sg.Button('Restart', key='-STOPWATCH 2 RESTART-')],
            ]
            , justification='center')
    ]

    right_main = [
        [sg.Column([[sg.Text('Statistics', font=('Helvetica', 20))]], justification='center')],
        stats,
        [sg.HSeparator()],
        [sg.Column([[sg.Text('Automations', font=('Helvetica', 20))]], justification='center')],
        [sg.Button('Clean Up Cards'), sg.Button('Run Daily Reset'), sg.Button('Run Monday Reset'),
         sg.Button('Run Thursday Reset')],
        [sg.HSeparator()],
        [sg.Column([[sg.Text('Stopwatches', font=('Helvetica', 20))]], justification='center')],
        stopwatch
    ]

    layout = [
        [
            sg.Column(top_nav_bar_left, vertical_alignment='top', size=(250, 80)),
            sg.VSeparator(),
            sg.Column(top_nav_bar_middle, vertical_alignment='top', size=(250, 80)),
            sg.VSeparator(),
            sg.Column(top_nav_bar_right, size=(260, 80), vertical_alignment='center')
        ],
        [sg.HSeparator()],
        [
            sg.Column(lists, vertical_alignment='top'),
            sg.VSeparator(),
            sg.Column(right_main, vertical_alignment='top')
        ]
    ]

    return sg.Window("Vexilar's Maple Helper", layout)


def open_gui():
    global selected_list, sett
    window = make_window()
    stopwatch = [False, None, False, None]

    # First Run
    try:
        sett = GeneralSettings.select().get()
        if sett.trello_api is None or sett.trello_api == '' or sett.trello_token is None or sett.trello_token == '' or sett.board_id is None or sett.board_id == '':
            settings.open_settings()
    except:
        settings.open_settings()

    window.read(timeout=1)
    update_gui(window)

    while True:
        event, values = window.read(timeout=1000)

        # Update Server Time
        window['-SERVER TIME-'].update(datetime.utcnow().strftime("%a, %b %d %I:%M:%S %p"))
        ursus_time(window)
        update_reset(window)

        # Stopwatch 1
        if stopwatch[0]:
            window['-STOPWATCH 1-'].update(str(datetime.now() - stopwatch[1]).split('.')[0])
        if stopwatch[2]:
            window['-STOPWATCH 2-'].update(str(datetime.now() - stopwatch[3]).split('.')[0])

        match event:
            case '-SETTINGS-':
                if settings.open_settings():
                    update_gui(window)
            case '-LISTS-':
                selected_list = Lists.select().where(Lists.name == values['-LISTS-'][0]).get()
            case '-DELETE LIST-':
                if selected_list is None:
                    sg.popup_error('You must select a list first')
                    continue
                response = sg.popup_ok_cancel('You are about to delete the list "%s"' % selected_list.name)
                if response == 'OK':
                    Lists.delete().where(Lists.list_id == selected_list.list_id).execute()
                    update_gui(window)
            case '-LIST SETTINGS-':
                if list_settings.open_settings(selected_list):
                    update_gui(window)
            case '-CUSTOM TASKS-':
                custom_tasks.open_tasks()
            case 'Clean Up Cards':
                scripts.cleanup_cards()
            case 'Run Daily Reset':
                scripts.run_automation(-1)
            case 'Run Monday Reset':
                scripts.run_automation(0)
            case 'Run Thursday Reset':
                scripts.run_automation(3)
            case '-OPEN TRELLO-':
                sett = GeneralSettings.select().get()
                webbrowser.open(sett.board_url)
            case '-STOPWATCH 1 STOP-' | '-STOPWATCH 2 STOP-':
                stopwatch_index = 0
                if '2' in event:
                    stopwatch_index = 2

                stopwatch[0 + stopwatch_index] = False
                stopwatch[1 + stopwatch_index] = datetime.now() - stopwatch[1 + stopwatch_index]
            case '-STOPWATCH 1 RESTART-' | '-STOPWATCH 2 RESTART-':
                stopwatch_index = 0
                if '2' in event:
                    window['-STOPWATCH 2-'].update('0:00:00')
                    stopwatch_index = 2
                else:
                    window['-STOPWATCH 1-'].update('0:00:00')
                    stopwatch_index = 0

                stopwatch[0 + stopwatch_index] = False
                stopwatch[1 + stopwatch_index] = None

                window['-STOPWATCH 1-'].update('0:00:00')
            case '-STOPWATCH 1 START-' | '-STOPWATCH 2 START-':
                stopwatch_index = 0
                if '2' in event:
                    stopwatch_index = 2

                if stopwatch[0 + stopwatch_index] is False and stopwatch[1 + stopwatch_index] is not None:
                    stopwatch[0 + stopwatch_index] = True
                    time = str(stopwatch[1 + stopwatch_index]).split('.')[0].split(':')
                    stopwatch[1 + stopwatch_index] = datetime.now() - timedelta(hours=int(time[0]),
                                                                                minutes=int(time[1]),
                                                                                seconds=int(time[2]))
                else:
                    stopwatch[0 + stopwatch_index] = True
                    stopwatch[1 + stopwatch_index] = datetime.now()

        if event == "OK" or event == sg.WIN_CLOSED:
            break

    window.close()


def update_reset(window):
    now = datetime.utcnow()
    window['-DAILY RESET-'].update(str(utli.daily() - now).split('.')[0])
    window['-MONDAY RESET-'].update(str(utli.on_day(0) - now).split('.')[0])
    window['-THURSDAY RESET-'].update(str(utli.on_day(3) - now).split('.')[0])


def ursus_time(window):
    first_time = [datetime.utcnow().replace(hour=1, minute=0, second=0),
                  datetime.utcnow().replace(hour=5, minute=0, second=0)]
    second_time = [datetime.utcnow().replace(hour=18, minute=0, second=0),
                   datetime.utcnow().replace(hour=22, minute=0, second=0)]
    utc_time = datetime.utcnow()

    # Add day if after last ursus
    if utc_time.hour >= second_time[1].hour:
        first_time[0] = first_time[0] + timedelta(days=1)
        first_time[1] = first_time[1] + timedelta(days=1)

    # Check if its currently ursus time
    if first_time[0] < utc_time < first_time[1]:
        window['-URSUS-'].update('Ursus is now! Ends in %s' % (str(first_time[1] - utc_time).split('.')[0]),
                                 text_color='orange')
        return
    elif second_time[0] < utc_time < second_time[1]:
        window['-URSUS-'].update('Ursus is now! Ends in %s' % (str(second_time[1] - utc_time).split('.')[0]),
                                 text_color='orange')
        return

    # Determine next ursus time
    if utc_time < first_time[0]:
        window['-URSUS-'].update('Ursus next in %s' % (str(first_time[0] - utc_time).split('.')[0]), text_color='white')
        return
    elif utc_time < second_time[0]:
        window['-URSUS-'].update('Ursus next in %s' % (str(second_time[0] - utc_time).split('.')[0]),
                                 text_color='white')
        return


def update_gui(window):
    global sett
    sett = GeneralSettings.select().get()
    boss_stats = statistics.get_boss_crystal_stats()
    task_count = statistics.get_task_amounts()

    window['-LISTS-'].update(values=database.get_lists())
    window['-STOPWATCH 1 NAME-'].update(sett.stopwatch_one_name)
    window['-STOPWATCH 2 NAME-'].update(sett.stopwatch_two_name)
    window['-BOSS CRYSTAL USED-'].update(boss_stats[0])
    window['-BOSS CRYSTAL REMAINING-'].update(boss_stats[1])
    window['-MESOS-'].update(boss_stats[2])
    window['-BOSS MESOS-'].update(boss_stats[3])
    window['-DAILY TASKS-'].update(task_count[0])
    window['-MONDAY TASKS-'].update(task_count[1])
    window['-THURSDAY TASKS-'].update(task_count[2])


open_gui()
