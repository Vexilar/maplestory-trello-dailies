import datetime

import PySimpleGUI as sg
import trello_api
import _pickle as cPickle
import utli
import pytz
from database import *
from datetime import datetime, timedelta


def cleanup_cards():
    data = trello_api.get_cards()
    cards = []
    now = datetime.now(tz=pytz.utc)

    print(now.tzinfo)

    for val in data:
        if val.get('dueComplete'):
            cards.append(val.get('id'))
        elif val.get('due'):
            due_date = datetime.fromisoformat(val.get('due'))
            if due_date < now:
                cards.append(val.get('id'))

    layout = [[sg.ProgressBar(100, orientation='h', expand_x=True, size=(20, 20), key='-PBAR-')]]
    window = sg.Window('Cleaning up cards', layout)

    while True:
        event, values = window.read(timeout=100)

        index = 1
        card_amount = len(cards)
        for card in cards:
            percent = int((index / card_amount) * 100)
            window['-PBAR-'].update(percent)
            trello_api.delete_card(card)
            index += 1

        window.close()
        break

    window.close()


def run_automation(day):
    lists = Lists.select()

    layout = [[sg.ProgressBar(100, orientation='h', expand_x=True, size=(20, 20), key='-PBAR-')]]
    window = sg.Window('Running %s Reset' % utli.get_day_name(day), layout)

    # Get amount of cards to add
    amount = 0
    for li in lists:
        if li.settings is None:
            continue

        data = cPickle.loads(li.settings)
        for task_list, values in data.items():
            # Custom Tasks
            if task_list == 'Custom Tasks':
                tasks = CustomTasks.select()
                custom_tasks = values.get('tasks')
                for task in tasks:
                    if utli.get_day(task.day) != day:
                        continue
                    if task.name in custom_tasks:
                        amount += 1

            if values.get('day') != day:
                continue
            amount += len(values.get('tasks'))

    while True:
        event, values = window.read(timeout=100)

        index = 1
        for li in lists:
            if li.settings is None:
                continue

            data = cPickle.loads(li.settings)
            for task_list, values in data.items():
                # Handle custom tasks
                if task_list == 'Custom Tasks':
                    tasks = CustomTasks.select()
                    custom_tasks = values.get('tasks')
                    for task in tasks:
                        if utli.get_day(task.day) != day:
                            continue
                        if task.name in custom_tasks:
                            trello_api.create_card(li.list_id, task.name, task.label_id, day, task.pos)
                            percent = int((index / amount) * 100)
                            window['-PBAR-'].update(percent)
                            index += 1

                # Check to see if day matches
                if values.get('day') != day:
                    continue

                # Create card and check list if checklist is true
                checklist = values.get('checklist')
                if checklist:
                    card_id = trello_api.create_card(li.list_id, task_list, values.get('label'), day)
                    checklist = trello_api.create_checklist(card_id, task_list)

                # Create tasks
                tasks = values.get('tasks')
                for task_name, difficulty in tasks.items():
                    if difficulty is not None:
                        task = '%s %s' % (difficulty, task_name)
                    else:
                        task = task_name

                    if checklist:
                        trello_api.create_checklist_item(checklist, task)
                    else:
                        trello_api.create_card(li.list_id, task, values.get('label'), day)

                    percent = int((index / amount) * 100)
                    window['-PBAR-'].update(percent)
                    index += 1

        window.close()
        break

    window.close()
