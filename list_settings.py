import PySimpleGUI as sg
import json
import _pickle as cPickle

import database
from database import *


def make_window(trello_list):
    # Load Json File
    data = json.load(open('resources/tasks.json'))

    layout = [
        [],
        [sg.HSeparator()],
        [sg.Column([[sg.Button('Save', size=(10, 2))]], justification='center')]
    ]

    labels = ['None'] + database.get_labels()

    # Create Interface
    for task_list, tasks_data in data.items():
        column = [
            [sg.Text(task_list, font=('Helvetica', 20))],
            [sg.Text('Trello Label', size=(14, 1)),
             sg.Combo(labels, key='-%s TRELLO LABEL-' % task_list, size=(15, 1))]
        ]

        # Add items to the column
        size = tasks_data.get('size')
        tasks = tasks_data.get('tasks')
        for task_name, value in tasks.items():
            if size == -1:
                insert = [sg.Checkbox(task_name, key='-%s-' % task_name)]
            else:
                insert = [sg.Checkbox(task_name, key='-%s-' % task_name, size=(size, 1))]

            # Add options
            if isinstance(value, dict):
                options = []
                for option, meso in value.items():
                    options.append(option)
                insert.append(sg.Combo(options, key='-%s Difficulty-' % task_name, size=(15, 1)))

            column.append(insert)

        # Add our newly created column to the list
        layout[0].append(sg.Column(column, vertical_alignment='top'))
        layout[0].append(sg.VSeparator())

    # Custom Task
    tasks = CustomTasks.select()
    if len(tasks) > 0:
        column = [[sg.Text('Custom Tasks', font=('Helvetica', 20))]]

        for task in tasks:
            column.append([sg.Checkbox(task.name, key='-%s-' % task.name)])

        layout[0].append(sg.Column(column, vertical_alignment='top'))
        layout[0].append(sg.VSeparator())

    # Remove last VSeperator
    layout[0] = layout[0][:-1]

    return sg.Window('%s Settings' % trello_list.name, layout)


def open_settings(trello_list):
    window = make_window(trello_list)

    # Load Settings
    window.read(timeout=1)
    load_settings(window, trello_list)

    # Create an event loop
    while True:
        event, values = window.read()

        if event == 'Save':
            if save_settings(trello_list, values):
                window.close()
                break
            else:
                sg.popup_error('You must select a difficulty when enabling a boss.')

        if event == "OK" or event == sg.WIN_CLOSED:
            break

    window.close()
    return True


def load_settings(window, trello_list):
    if trello_list.settings is None:
        return
    data = cPickle.loads(trello_list.settings)

    # Cycle through all lists
    for task_list, tasks_data in data.items():
        # Update label
        if tasks_data.get('label') is not None:
            label = Labels.select().where(Labels.label_id == tasks_data.get('label')).get().name
            window.find_element('-%s TRELLO LABEL-' % task_list, silent_on_error=True).update(value=label)

        # Cycle through tasks
        tasks = tasks_data.get('tasks')
        for task_name, difficulty in tasks.items():
            # Update task to enabled
            window.find_element('-%s-' % task_name, silent_on_error=True).update(value=True)

            # Check if the task has a difficulty
            if difficulty is not None:
                window.find_element('-%s Difficulty-' % task_name, silent_on_error=True).update(value=difficulty)


def save_settings(trello_list, values):
    # Load Json File
    data = json.load(open('resources/tasks.json'))
    save_data = {}

    # Create save data
    for task_list, tasks_data in data.items():
        save = {}

        # Set Label
        if values['-%s TRELLO LABEL-' % task_list] != '' and values['-%s TRELLO LABEL-' % task_list] != 'None':
            save['label'] = Labels.select().where(Labels.name == values['-%s TRELLO LABEL-' % task_list]).get().label_id

        save['day'] = tasks_data.get('day')
        save['checklist'] = tasks_data.get('checklist')

        # Gather what tasks to save
        task_saves = {}
        tasks = tasks_data.get('tasks')
        for task_name, value in tasks.items():
            # Check if task is enabled
            if not values['-%s-' % task_name]:
                continue

            # Check if task has a difficultly
            if isinstance(value, dict):
                if values['-%s Difficulty-' % task_name] == '':
                    return False

                # Generate save
                difficulty = values['-%s Difficulty-' % task_name]
                task_saves['%s' % task_name] = difficulty
            else:
                task_saves['%s' % task_name] = None

        # Save task
        if len(task_saves) > 0:
            save['tasks'] = task_saves
            save_data[task_list] = save

    # Custom Tasks
    tasks = CustomTasks.select()
    task_saves = {}

    for task in tasks:
        if values['-%s-' % task.name]:
            task_saves['%s' % task.name] = None

    if len(task_saves) > 0:
        save = {'tasks': task_saves}
        save_data['Custom Tasks'] = save

    trello_list.settings = cPickle.dumps(save_data)
    trello_list.save()
    return True

