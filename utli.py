from datetime import datetime, timedelta


def daily():
    time = datetime.utcnow()
    time = time + timedelta(days=1)
    time = time.replace(hour=0, minute=0, second=0)
    return time


def on_day(day):
    date = datetime.utcnow()
    days = (day - date.weekday() + 7) % 7
    if days == 0:
        days = 7
    date = date + timedelta(days=days)
    date = date.replace(hour=0, minute=0, second=0)
    return date


def get_date(day):
    if day == -1:
        date = daily()
    else:
        date = on_day(day)
    return date


def get_day(day):
    match day:
        case 'Daily':
            return -1
        case 'Monday':
            return 0
        case 'Thursday':
            return 3


def get_day_name(day):
    match day:
        case -1:
            return 'Daily'
        case 0:
            return 'Monday'
        case 3:
            return 'Thursday'
