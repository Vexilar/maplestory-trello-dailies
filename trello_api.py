import requests
import json
import utli
from database import *


def submit_post_request(url, data=None, request_type='POST'):
    if data is None:
        data = {}
    headers = {
        "Accept": "application/json"
    }

    settings = GeneralSettings.select().get()

    # Set API Key and Token
    data['key'] = settings.trello_api
    data['token'] = settings.trello_token

    response = requests.request(request_type, url, headers=headers, params=data)
    return json.loads(response.text)


def submit_request(url, data=None, request_type='GET'):
    if data is None:
        data = {}
    settings = GeneralSettings.select().get()

    # Set API Key and Token
    data['key'] = settings.trello_api
    data['token'] = settings.trello_token

    response = requests.request(request_type, url, params=data)
    return json.loads(response.text)


def create_card(list_id, name, label, day, pos=None):
    url = "https://api.trello.com/1/cards"

    data = {
        'idList': list_id,
        'name': name,
        'idLabels': label,
        'pos': pos,
        'due': utli.get_date(day)
    }
    return submit_post_request(url, data)['id']


def create_checklist(card_id, name):
    url = "https://api.trello.com/1/cards/%s/checklists" % card_id

    data = {
        'name': name
    }
    return submit_post_request(url, data)['id']


def create_checklist_item(checklist_id, name):
    url = "https://api.trello.com/1/checklists/%s/checkItems" % checklist_id
    data = {
        'name': name
    }
    return submit_post_request(url, data)


def get_cards():
    settings = GeneralSettings.select().get()
    url = "https://api.trello.com/1/boards/%s/cards/open" % settings.board_id
    return submit_request(url)


def delete_card(card_id):
    url = "https://api.trello.com/1/cards/%s" % card_id
    return submit_request(url, request_type='DELETE')


def get_board():
    settings = GeneralSettings.select().get()
    url = "https://api.trello.com/1/boards/%s" % settings.board_id
    return submit_request(url)


def get_lists():
    settings = GeneralSettings.select().get()
    url = "https://api.trello.com/1/boards/%s/lists" % settings.board_id
    return submit_request(url)


def get_labels():
    settings = GeneralSettings.select().get()
    url = "https://api.trello.com/1/boards/%s/labels" % settings.board_id
    return submit_request(url)


def delete_board(board_id):
    url = "https://api.trello.com/1/boards/%s" % board_id
    return submit_request(url, request_type='DELETE')


def create_board(name):
    url = "https://api.trello.com/1/boards/"
    data = {
        'name': name
    }
    return submit_post_request(url, data)


def move_list(card_id, board_id):
    url = "https://api.trello.com/1/lists/%s/idBoard" % card_id
    data = {
        'value': board_id
    }
    return submit_post_request(url, data, request_type='PUT')


def create_list(name):
    settings = GeneralSettings.select().get()
    url = "https://api.trello.com/1/lists"
    data = {
        'name': name,
        'idBoard': settings.board_id
    }
    return submit_post_request(url, data)

