from peewee import *

db = SqliteDatabase('resources/database.db')


class BaseModel(Model):
    class Meta:
        database = db


class GeneralSettings(BaseModel):
    trello_api = TextField(null=True)
    trello_token = TextField(null=True)
    board_id = TextField(null=True)
    board_url = TextField(null=True)
    stopwatch_one_name = TextField(default='Stopwatch 1')
    stopwatch_two_name = TextField(default='Stopwatch 2')


class Lists(BaseModel):
    list_id = TextField(unique=True)
    name = TextField()
    settings = BlobField(null=True)


class Labels(BaseModel):
    label_id = TextField(unique=True)
    name = TextField()


class CustomTasks(BaseModel):
    name = TextField(unique=True)
    day = TextField()
    label_id = TextField(null=True)
    pos = TextField(null=True)


def get_lists():
    lists = []
    results = Lists.select()
    for li in results:
        lists.append(li.name)
    return lists


def get_labels():
    labels = []
    results = Labels.select()
    for label in results:
        labels.append(label.name)
    return labels


def get_custom_tasks():
    tasks = []
    results = CustomTasks.select()
    for task in results:
        tasks.append(task.name)
    return tasks


db.create_tables([GeneralSettings, Lists, Labels, CustomTasks])
